# UnitTest
***Pruebas Unitarias - Agenda 5***

En la clase `Calculator` se crearon los métodos para restar, multiplicar, dividir, sacar la raíz cuadrada de un número en específico y la regla de tres, la cuál consiste en encontrar la variable *Y* por medio de *A, B, Z*. Además se agregaron las pruebas correspondientes para cada uno de estos métodos en la clase `CalculatorTest`

**Integrantes**
 - Amara López Galo
 - Yosselin Peña Contreras
 - Leticia Téllez Fargas
 - Brian Téllez Hernández
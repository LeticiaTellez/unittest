﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Calculator
    {
        public int Add(int value1, int value2)
        {
            return value1 + value2;
        }

        public int Subtract(int value1, int value2)
        {
            return value1 - value2;
        }

        public int Multiply(int value1, int value2)
        {
            return value1 * value2;
        }

        public int Divide(int value1, int value2)
        {
            return value1 / value2;
        }

        public double SquareRoot(int value)
        {
            return Math.Sqrt(value);
        }

        public int RuleThree(int valueA, int valueB, int valueX)
        {
            int valueY = ((valueB * valueX) / valueA);
            return valueY;
        }
    }
}

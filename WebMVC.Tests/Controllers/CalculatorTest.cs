﻿using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMVC.Tests.Controllers
{

    [TestClass]
    public class CalculatorTest
    {
        
        [TestInitialize]
        public void OnTestInitialize()
        {
            _SystemUnderTest = null;
        }

        private Calculator _SystemUnderTest;

        public Calculator SystemUnderTest
        {
            get
            {
                if(_SystemUnderTest == null)
                {
                    _SystemUnderTest = new Calculator();
                }
                return _SystemUnderTest;
            }


        }

        [TestMethod]
        public void Add()
        {

            //Arrange (Organizar)
            int value1 = 2;
            int value2 = 3;
            int expected = 5;

            //Act (Actuar)


            int actual = SystemUnderTest.Add(value1,value2);

            //Assert (Afirmar)
            Assert.AreEqual<int>(expected, actual, "Error, valores no coinciden");

        }

        [TestMethod]
        public void Subtract()
        {
            //INICIALIZACIÓN DE VALORES
            int value1 = 30;
            int value2 = 21;
            int expected = 9;

            //OBTENIENE EL RESULTADO DEL MÉTODO DE LA CLASE CALCULATOR
            int actual = SystemUnderTest.Subtract(value1, value2);

            //VERIFICA SI EL RESULTADO DEL MÉTODO COINCIDE CON EL RESULTADO ESPERADO
            Assert.AreEqual<int>(expected, actual, "Error, valores no coinciden");
        }

        [TestMethod]
        public void Multiply()
        {
            //INICIALIZACIÓN DE VALORES
            int value1 = 9;
            int value2 = 12;
            int expected = 108;

            //OBTENIENE EL RESULTADO DEL MÉTODO DE LA CLASE CALCULATOR
            int actual = SystemUnderTest.Multiply(value1, value2);

            //VERIFICA SI EL RESULTADO DEL MÉTODO COINCIDE CON EL RESULTADO ESPERADO
            Assert.AreEqual<int>(expected, actual, "Error, valores no coinciden");
        }

        [TestMethod]
        public void Divide()
        {
            //INICIALIZACIÓN DE VALORES
            int value1 = 81;
            int value2 = 3;
            int expected = 27;

            //OBTENIENE EL RESULTADO DEL MÉTODO DE LA CLASE CALCULATOR
            int actual = SystemUnderTest.Divide(value1, value2);

            //VERIFICA SI EL RESULTADO DEL MÉTODO COINCIDE CON EL RESULTADO ESPERADO
            Assert.AreEqual<int>(expected, actual, "Error, valores no coinciden");
        }

        [TestMethod]
        public void SquareRoot()
        {
            //INICIALIZACIÓN DE VALORES
            int value1 = 3844;
            double expected = 62;

            //OBTENIENE EL RESULTADO DEL MÉTODO DE LA CLASE CALCULATOR
            double actual = SystemUnderTest.SquareRoot(value1);

            //VERIFICA SI EL RESULTADO DEL MÉTODO COINCIDE CON EL RESULTADO ESPERADO
            Assert.AreEqual<double>(expected, actual, "Error, valores no coinciden");
        }

        [TestMethod]
        public void RuleThree()
        {
            //INICIALIZACIÓN DE VALORES
            int valueA = 2;
            int valueB = 4;
            int valueX = 3;
            int expected = 6;

            //OBTENIENE EL RESULTADO DEL MÉTODO DE LA CLASE CALCULATOR
            int actual = SystemUnderTest.RuleThree(valueA, valueB, valueX);

            //VERIFICA SI EL RESULTADO DEL MÉTODO COINCIDE CON EL RESULTADO ESPERADO
            Assert.AreEqual<int>(expected, actual, "Error, valores no coinciden");
        }
    }
}
